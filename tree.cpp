/*
   PP3 is a menu driven project
   that allows the user to modify
   a pre­built binary tree.
   The nodes will be
   <30,10,45,38,20,50,25,33,8,12>
   */
 
#include <iostream>
#include <iomanip>
using namespace std;

//Struct defining a tree node

struct binaryTree{
	int key;

		binaryTree * parent;

		binaryTree * left;

		binaryTree * right;

};

//prototype methods

void display_menu();

void display_pre_order(binaryTree * root);

void display_in_order(binaryTree * root);

void display_post_order(binaryTree * root);

void tree_insert(binaryTree * root, binaryTree * new_node);

binaryTree * tree_delete(binaryTree * root, binaryTree * del_node);

binaryTree * tree_search(binaryTree * root, int key);

binaryTree * tree_successor(binaryTree * root);

binaryTree * tree_min(binaryTree * root);

 

//main method

int main(){

	string choice;

		 int key_value;

		    //initialize the BST

		    int keys[] = {30,10,45,38,20,50,25,33,8,12};

		    binaryTree * root = new binaryTree;

		   root­>key = 30;

		 

		  //add nodes to tree

		              for(int i = 1; i < 10; i ++){

			                            binaryTree * addr = new binaryTree();

				                            addr­>key = keys[i];

				                            addr­>left = NULL;

				                            addr­>right = NULL;

				                            addr­>parent = NULL;

				                            tree_insert(root,addr);

				              }

	 

		              //show the menu for the user

		              display_menu();

		              cin >> choice;

		              while(choice != "q" || choice != "Q"){

			                            //insert a new node

				                            if ( choice == "a" || choice == "A" ){

					                                          binaryTree * node = new binaryTree();

						                                          cout << "Eneter a key value for insertion\n";

						                                          cin >> key_value;

						                                          node­>key = key_value;

						                                          tree_insert(root,node);

						                            }

			                            //delete a node

				                            else if ( choice == "b" || choice == "B" ){

					                                          binaryTree * node = new binaryTree();

						                                          cout << "Enter a key value for deletion:\n";

						                                          cin >> key_value;

						                                          node = tree_search(root,key_value);

						                                          if(node != NULL)

						                                                        tree_delete(root,node);

						                                          else

						                                                        cout << "No node with that key.\n\n";

						                            }

			                            //search for a node, display message for found/not found

				                            else if ( choice == "c" || choice == "C" ){

					                                          binaryTree * node = new binaryTree();

						                                          cout << "Enter a key value to search\n";

						                                          cin >> key_value;

						                                          node = tree_search(root,key_value);

						                                          if(node == NULL){

							                                                        cout << "Key is not in the tree\n\n";

								                                          }

					                                          else{

						                                                        cout << "Key found.\n\n";

							  }

					  }

					                            //Display the tree in pre­order

						                            else if ( choice == "d" || choice == "D" ){

							                                          cout << "Display Pre­Order:\n";

								                                          display_pre_order(root);

								                                          cout << "\n\n";

								                            }

					                            //Display the tree in in­order

						                            else if ( choice == "e" || choice == "E" ){

							                                          cout << "Display In­Order:\n";

								                                          display_in_order(root);

								                                          cout << "\n\n";

								                            }

					                            //Display the tree in post­order format

						                            else if ( choice == "f" || choice == "F" ){

							                                          cout << "Display Post­Order:\n";

								                                          display_post_order(root);

								                                          cout << "\n\n";

								                            }

					                            //Quit the program

						                            else if ( choice == "q" || choice == "Q" ){

							                                          return 1;

								                            }

					                            //Show message when nothing is selected

						                            else { cout << "Nothing Selected\n"; }

					                            //display menu and get next choice

						                            display_menu();

						                            cin >> choice;

						              }

					              return 0;

}

 

//method to display the menu

void display_menu(){

	              cout << "Select an option\n"

		                            << "\tA ­ Insert\n"

		                            << "\tB ­ Delete\n"

		                            << "\tC ­ Search\n"

		                            << "\tD ­ Display Pre­Order\n"

		                            << "\tE ­ Display In­Order\n"

		                            << "\tF ­ Display Post­Order\n"

		                            << "\tQ ­ Quit\n";

}

 

//method to output the tree pre­order

void display_pre_order(binaryTree * root){

	              if(root != NULL){

		                            cout << root ­> key << " ";

			                            display_pre_order(root ­> left);

			                            display_pre_order(root ­> right);

			              }

}

 

//method to output the tree in­order

void display_in_order(binaryTree * root){

	              if(root != NULL){

		                            display_in_order(root ­> left);

			                            cout << root ­> key << " ";

			                            display_in_order(root ­> right);

			              }

}

 

//method to output the tree post­order

void display_post_order(binaryTree * root){

	              if(root != NULL){

		                            display_post_order(root ­> left);

			                            display_post_order(root ­> right);

			                            cout << root ­> key << " ";

			              }

}

 

//method to insert a new node

void tree_insert(binaryTree * root, binaryTree * new_node){

	              binaryTree *y = NULL;

		              binaryTree *x = root;

		              while( x != NULL){

			                            y = x;

				                            if( new_node ­> key < x ­> key ){ x = x ­> left; }

			                            else { x = x ­> right; }

			              }

			              new_node ­> parent = y;

				              if(y==NULL){root = new_node;}

			              else if (new_node ­> key < y ­> key){ y ­> left = new_node;}

			              else { y ­> right = new_node;}

}

 

//method to delete a node

binaryTree * tree_delete(binaryTree * root, binaryTree * del_node){

	              binaryTree * x = NULL;

		  binaryTree * y = NULL;

		  if(del_node ­> left == NULL || del_node ­> right == NULL)

		                            y = del_node;

		              else

		                            y = tree_successor(del_node);

		              if(y ­> left != NULL)

		                            x = y­>left;

		              else

		                            x = y­>right;

		              if(x != NULL)

		                            x­>parent = y­>parent;

		              if(y­>parent == NULL)

		                            root = x;

		              else if( y == y­>parent­>left )

		                            y­>parent­>left = x;

		              else

		                            y­>parent­>right = x;

		              if( y != del_node )

		                            del_node­>key = y­>key;

		              return y;

}

 

//tree successor method used with tree delete

binaryTree * tree_successor(binaryTree * root){

	              if(root­>right != NULL){

		                            return tree_min(root­>right);

			              }

	              binaryTree * y = root­>parent;

		              while(y != NULL && root == y­>right){

			                            root = y;

				                            y = y­>parent;

				              }

	              return y;

}

 

//tree min used with tree successor

binaryTree * tree_min(binaryTree * root){

	              while(root­>left != NULL ){

		                            root = root­>left;

			              }

	              return root;

}

 

//tree search method

binaryTree * tree_search(binaryTree * root, int key){

	  if( root == NULL || key == root ­> key){

		  return root;

			              }

	              if(key < root­>key){

		                            if(root­>left != NULL)

			                                          return tree_search(root­>left, key);

			                            else

			                                          return NULL;

			              }

	              else{

		                            if(root­>right != NULL)

			                                          return tree_search(root­>right, key);

			                            else

			                                          return NULL;

			              }

}
